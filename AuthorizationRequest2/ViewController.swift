//
//  ViewController.swift
//  AuthorizeRemoteNotifications
//
//  Created by Frans Glorie on 08/11/2019.
//  Copyright © 2019 Frans Glorie. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    // Constants
    let alreadyOnboardedKey = "alreadyOnboarded"

    
    // Outlets
    @IBOutlet weak var notificationSwitchOutlet: UISwitch!
    @IBAction func notificationSwitch(_ sender: UISwitch) {
        
        if sender.isOn {
            print("FG switched on")
            
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { granted, error in
                // Enable or disable features based on authorization.
                self.logInfo(msg: granted ? "Granted" : "Not granted")
                
                if !granted {
                    // Alert, set switch off, send user to settings
                    self.logInfo(msg: "Can't switch on, because app has no permissions for receiving notifications. Change app settings on iOS-level first.")
                    DispatchQueue.main.async {
                        sender.isOn = false
                        
                        self.showAlert(msg: "Can't switch on, because app has no permissions for receiving notifications. Change app settings on iOS-level first.") { response in
                            
                            if let url = URL(string: UIApplication.openSettingsURLString) {
                                UIApplication.shared.open(url, options: [:], completionHandler: { success in
                                    self.logInfo(msg: "Completed opening settings. Success: \(success)")
                                })
                            }
                        }
                    }
                }
            }
        } else {
            logInfo(msg: "Switching off is always ok.")
        }
    }
    
    @IBOutlet weak var getNotificationsTextView: UITextView!
    
    @IBAction func onboardButton(_ sender: UIButton) {

        // First registration
        if !UserDefaults.standard.bool(forKey: alreadyOnboardedKey) {
            self.logInfo(msg: "Onboarding started")
            UserDefaults.standard.set(true, forKey: alreadyOnboardedKey)
            
            let ac = UIAlertController(title: "Onboarding flow", message: "Do you want to enable notifications?", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Yes", style: .default) { action in
                
                let center = UNUserNotificationCenter.current()
                center.requestAuthorization(options: [.alert, .sound]) { granted, error in
                    // Enable or disable features based on authorization.
                    self.logInfo(msg: granted ? "Granted" : "Not granted")
                    DispatchQueue.main.async {
                        self.notificationSwitchOutlet.isOn = granted
                    }
                }
            }
            ac.addAction(yesAction)
            let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            ac.addAction(noAction)
            self.present(ac, animated: true) {
                print("FG present complete")
            }
        } else {
            self.logInfo(msg: "Onboarding has already been done")
        }
    }
    
    @IBAction func getNotificationsAuthorizationButton(_ sender: UIButton) {
        
        self.logInfo(msg: "")
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { settings in
            
            self.logInfo(msg: "Settings: \n\(settings.description)")
            
            guard settings.authorizationStatus == .authorized else {
                print("FG not authorized")
                self.logInfo(msg: "Unauthorized")

                return
            }

            if settings.alertSetting == .enabled {
                // Schedule an alert-only notification.
                print("FG enabled")
                self.logInfo(msg: "Alerts enabled")
            } else {
                // Schedule a notification with a badge and sound.
                print("FG disabled")
                self.logInfo(msg: "Alerts disabled")
            }
        }
    }
    
    // Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getNotificationsTextView.text = "Start of log:\n"
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { settings in
        
            if settings.authorizationStatus == .authorized {
                DispatchQueue.main.async {
                    self.notificationSwitchOutlet.isOn = true
                }
            } else {
                DispatchQueue.main.async {
                    self.notificationSwitchOutlet.isOn = false
                }
            }
        }
    }
    
    func setNotificationsSwitch() {
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { settings in
            
            DispatchQueue.main.async {
                self.notificationSwitchOutlet.isOn = settings.authorizationStatus == .authorized
            }
        }
    }
    
    func logInfo(msg: String) {
        DispatchQueue.main.async {
            var timestamp: String = ""
            if !msg.isEmpty {
                timestamp = "\(Date()) - "
            }
            self.getNotificationsTextView.text.append("\(timestamp) \(msg)\n")
            let range = NSRange(location: self.getNotificationsTextView.text.count - 1, length: 1)
            self.getNotificationsTextView.scrollRangeToVisible(range)
        }
    }
    
    func showAlert(msg: String, completionHandler: ((UIAlertAction) -> Void)? ) {

        let ac = UIAlertController(title: "Message", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: completionHandler)
        ac.addAction(okAction)
        self.present(ac, animated: true) {
            self.logInfo(msg: "FG present complete")
        }
        logInfo(msg: "FG exit showAlert function")
    }
}

